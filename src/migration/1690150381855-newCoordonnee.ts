import { MigrationInterface, QueryRunner } from 'typeorm';

import * as csv from 'csv-parser';
import { Coordonnee } from '../coordonnees/entities/coordonnee.entity';

const fs = require('fs');
const async = require('async');

function mapHeaders({ header, index }) {
  header = header.trim();

  switch (header) {
    case 'commune_ou_quartier_de_lactivite':
      return 'communeOuQuartierDeLactivite';
    case 'latitude':
      return 'latitude';
    case 'longitude':
      return 'longitude';
    default:
      return header;
  }
}
export class NewCoordonnee1690150381855 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const filePath = './src/migration/coordonne.csv';
    console.log(`Opening file at path: ${filePath}`);

    const coordonneeQueue = async.queue(async (row) => {
      const newQueryRunner = queryRunner.manager.connection.createQueryRunner();

      const coordonnee = newQueryRunner.manager
        .getRepository(Coordonnee)
        .create(row);

      try {
        await newQueryRunner.manager.getRepository(Coordonnee).save(coordonnee);
      } catch (e) {
        console.error('Erreur lors de la sauvegarde des coordonnées :', e);
      } finally {
        await newQueryRunner.release();
      }
    }, 5);

    fs.createReadStream(filePath)
      .on('error', (err) => {
        console.error('Error reading file:', err);
      })
      .pipe(csv({ mapHeaders, separator: ';' }))
      .on('data', (row) => {
        coordonneeQueue.push(row);
      })
      .on('end', () => {
        console.log('Fichier CSV traité avec succès');
      });
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
