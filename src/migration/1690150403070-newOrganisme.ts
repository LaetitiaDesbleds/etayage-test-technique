import { MigrationInterface, QueryRunner } from 'typeorm';

import * as csv from 'csv-parser';
import { Organisme } from '../organismes/entities/organisme.entity';
import { Coordonnee } from '../coordonnees/entities/coordonnee.entity';

const fs = require('fs');
const async = require('async');

// Cette fonction est utilisée pour mapper les en-têtes de colonnes
// du fichier CSV aux propriétés de la classe Organisme
function mapHeaders({ header, index }) {
  // Supprime les espaces blancs au début et à la fin de l'en-tête
  header = header.trim();

  // Selon l'en-tête de la colonne, renvoie le nom
  // correspondant de la propriété dans la classe Organisme
  switch (header) {
    case 'Nom organisme':
      return 'nomOrganisme';
    case 'Sigle':
      return 'sigle';
    case 'Complément nom':
      return 'complementNom';
    case 'Adresse postale':
      return 'adressePostale';
    case 'Complément adresse':
      return 'complementAdresse';
    case 'Boite postale':
      return 'boitePostale';
    case 'Code postal':
      return 'codePostal';
    case 'Commune':
      return 'commune';
    case 'Tel':
      return 'tel';
    case 'Tel2':
      return 'tel2';
    case 'E mail':
      return 'email';
    case 'Url':
      return 'url';
    case 'Description activité':
      return 'descriptionActivite';
    case 'Age en clair':
      return 'ageEnClair';
    case "Niveau de l'activité":
      return 'niveauActivite';
    case "Moment de l'activité":
      return 'momentActivite';
    case 'Tarif':
      return 'tarif';
    case 'Contact activité':
      return 'contactActivite';
    case 'Lieu activité':
      return 'lieuActivite';
    case 'Activité':
      return 'activite';
    case 'Paiements acceptés':
      return 'paiementsAcceptes';
    case 'age':
      return 'age';
    case 'niveau':
      return 'niveau';
    case 'commune_ou_quartier_de_lactivite':
      return 'communeOuQuartierDeLactivite';
    default:
      return header; // Si l'en-tête ne correspond à aucune des options ci-dessus, retourne l'en-tête original
  }
}

export class NewOrganisme1690150403070 implements MigrationInterface {
  // Cette méthode sert à migrer des données depuis un fichier CSV vers une base de données
  public async up(queryRunner: QueryRunner): Promise<void> {
    // Chemin vers le fichier CSV
    const filePath = './src/migration/loisirs-az-4bis.csv';
    console.log(`Opening file at path: ${filePath}`);

    // Crée une file d'attente pour traiter les organismes de manière asynchrone
    const organismeQueue = async.queue(async (row) => {
      // Crée une nouvelle instance de QueryRunner pour chaque organisme
      const newQueryRunner = queryRunner.manager.connection.createQueryRunner();

      // Transforme la ligne en objet Organisme
      const organisme = newQueryRunner.manager
        .getRepository(Organisme)
        .create(row);

      // Cherchez l'entrée correspondante dans la table coordonnee
      const coordonnee = await newQueryRunner.manager
        .getRepository(Coordonnee)
        .findOne({
          where: {
            communeOuQuartierDeLactivite: row.communeOuQuartierDeLactivite,
          },
        });

      // Si une coordonnee correspondante a été trouvée, utilisez son ID
      if (coordonnee) {
        (organisme as any).coordonneeId = coordonnee.id;
      } else {
        console.warn(
          `Aucune coordonnée trouvée pour l'organisme ${
            (organisme as any).nomOrganisme
          }`,
        );
      }

      try {
        // Tente de sauvegarder l'organisme dans la base de données
        await newQueryRunner.manager.getRepository(Organisme).save(organisme);
      } catch (e) {
        // Affiche l'erreur si la sauvegarde échoue
        console.error("Erreur lors de la sauvegarde de l'organisme :", e);
      } finally {
        // Libère le QueryRunner, qu'il y ait eu une erreur ou non
        await newQueryRunner.release();
      }
    }, 5); // nombre maximum de tâches simultanées dans la file d'attente

    // Ouvre le fichier CSV pour lecture
    fs.createReadStream(filePath)
      .on('error', (err) => {
        // Affiche l'erreur si la lecture du fichier échoue
        console.error('Error reading file:', err);
      })
      .pipe(csv({ mapHeaders, separator: ';' })) // Traite le fichier CSV une ligne à la fois, en utilisant le point-virgule comme séparateur de colonnes
      .on('data', (row) => {
        // Ajoute chaque ligne à la file d'attente pour traitement
        organismeQueue.push(row);
      })
      .on('end', () => {
        // Affiche un message lorsque le fichier a été entièrement traité
        console.log('Fichier CSV traité avec succès');
      });
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
