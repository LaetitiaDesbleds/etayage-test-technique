import { Module, OnModuleInit } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrganismesModule } from './organismes/organismes.module';
import { DataSource } from 'typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CoordonneesModule } from './coordonnees/coordonnees.module';
import typeorm from './config/typeorm';

@Module({
  imports: [
    // Importe le module de configuration avec une configuration globale et charge la configuration de TypeORM
    ConfigModule.forRoot({
      isGlobal: true,
      load: [typeorm],
    }),
    // Configure le module TypeOrm de manière asynchrone en utilisant la configuration de TypeORM
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) =>
        configService.get('typeorm'),
    }),
    OrganismesModule,
    CoordonneesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements OnModuleInit {
  constructor(private readonly connection: DataSource) {}

  // Exécute les migrations une fois le module initialisé
  async onModuleInit() {
    await this.connection.runMigrations();
  }
}
