import { Injectable } from '@nestjs/common';
import { CreateOrganismeDto } from './dto/create-organisme.dto';
import { UpdateOrganismeDto } from './dto/update-organisme.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Organisme } from './entities/organisme.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OrganismesService {
  constructor(
    @InjectRepository(Organisme)
    private readonly organismeRepository: Repository<Organisme>,
  ) {}

  async findAll(): Promise<Organisme[]> {
    return await this.organismeRepository.find();
  }

  async findOne(id: number): Promise<Organisme> {
    return await this.organismeRepository.findOne({
      where: {
        id,
      },
    });
  }

  async create(createOrganismeDto: CreateOrganismeDto) {
    const organsimeEntity = new Organisme();
    organsimeEntity.nomOrganisme = createOrganismeDto.nomOrganisme;
    organsimeEntity.sigle = createOrganismeDto.sigle;
    organsimeEntity.complementNom = createOrganismeDto.complementNom;
    organsimeEntity.adressePostale = createOrganismeDto.adressePostale;
    organsimeEntity.complementAdresse = createOrganismeDto.complementAdresse;
    organsimeEntity.boitePostale = createOrganismeDto.boitePostale;
    organsimeEntity.codePostal = createOrganismeDto.codePostal;
    organsimeEntity.commune = createOrganismeDto.commune;
    organsimeEntity.tel = createOrganismeDto.tel;
    organsimeEntity.tel2 = createOrganismeDto.tel2;
    organsimeEntity.email = createOrganismeDto.email;
    organsimeEntity.url = createOrganismeDto.url;
    organsimeEntity.descriptionActivite =
      createOrganismeDto.descriptionActivite;
    organsimeEntity.ageEnClair = createOrganismeDto.ageEnClair;
    organsimeEntity.niveauActivite = createOrganismeDto.niveauActivite;
    organsimeEntity.momentActivite = createOrganismeDto.momentActivite;
    organsimeEntity.tarif = createOrganismeDto.tarif;
    organsimeEntity.contactActivite = createOrganismeDto.contactActivite;
    organsimeEntity.lieuActivite = createOrganismeDto.lieuActivite;
    organsimeEntity.activite = createOrganismeDto.activite;
    organsimeEntity.paiementsAcceptes = createOrganismeDto.paiementsAcceptes;
    organsimeEntity.age = createOrganismeDto.age;
    organsimeEntity.niveau = createOrganismeDto.niveau;
    organsimeEntity.communeOuQuartierDeLactivite =
      createOrganismeDto.communeOuQuartierDeLactivite;
    const organisme = this.organismeRepository.create(organsimeEntity);
    await this.organismeRepository.save(organisme);
    return organisme;
  }

  async update(id: number, data: Partial<UpdateOrganismeDto>) {
    await this.organismeRepository.update({ id }, data);
    const organisme = this.organismeRepository.findOne({ where: { id } });
    return organisme;
  }

  async remove(id: number) {
    const organisme = await this.organismeRepository.findOne({ where: { id } });
    await this.organismeRepository.delete({ id });
    return organisme;
  }
}
