import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class CreateOrganismeDto {
  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly nomOrganisme: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly sigle: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly complementNom: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly adressePostale: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly complementAdresse: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly boitePostale: string;

  @ApiModelProperty({
    required: false,
    type: 'float',
  })
  readonly codePostal: number;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly commune: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly tel: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly tel2: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly email: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly url: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly descriptionActivite: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly ageEnClair: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly niveauActivite: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly momentActivite: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly tarif: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly contactActivite: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly lieuActivite: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly activite: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly paiementsAcceptes: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly age: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly niveau: string;

  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly communeOuQuartierDeLactivite: string;

  @ApiModelProperty({
    required: false,
    type: 'number',
  })
  readonly coordonneeId: number;
}
