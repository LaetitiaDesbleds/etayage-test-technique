import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
} from '@nestjs/common';
import { OrganismesService } from './organismes.service';
import { CreateOrganismeDto } from './dto/create-organisme.dto';
import { UpdateOrganismeDto } from './dto/update-organisme.dto';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('organismes')
@ApiTags('organismes')
export class OrganismesController {
  constructor(private readonly organismesService: OrganismesService) {}
  @Get()
  @ApiResponse({
    status: 201,
    description: 'La liste des organismes de loisirs',
  })
  @ApiResponse({
    status: 404,
    description: 'Pas de liste trouvée',
  })
  findAll() {
    return this.organismesService.findAll();
  }

  @Get('/:id')
  @ApiResponse({
    status: 201,
    description: "L'organisme de loisirs",
  })
  @ApiResponse({
    status: 404,
    description: "Pas d'organisme",
  })
  findOne(@Param('id') id: number) {
    return this.organismesService.findOne(id);
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: "L'organisme à bien été créé",
  })
  @ApiResponse({
    status: 404,
    description: "Erreur lors de la création de l'organisme",
  })
  create(@Body() createOrganismeDto: CreateOrganismeDto) {
    return this.organismesService.create(createOrganismeDto);
  }

  @Put('/:id')
  @ApiResponse({
    status: 201,
    description: "L'organisme à bien été modifié",
  })
  @ApiResponse({
    status: 404,
    description: "Erreur lors de la modification de l'organisme",
  })
  update(
    @Param('id') id: number,
    @Body() updateOrganismeDto: UpdateOrganismeDto,
  ) {
    return this.organismesService.update(id, updateOrganismeDto);
  }

  @Delete('/:id')
  @ApiResponse({
    status: 201,
    description: "L'organisme à bien été supprimé",
  })
  @ApiResponse({
    status: 404,
    description: "Erreur lors de la suppression de l'organisme",
  })
  remove(@Param('id') id: number) {
    return this.organismesService.remove(id);
  }
}
