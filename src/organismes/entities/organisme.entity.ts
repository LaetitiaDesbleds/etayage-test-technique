import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { IsEmail } from 'class-validator';
import { Coordonnee } from '../../coordonnees/entities/coordonnee.entity';

@Entity()
export class Organisme {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'text',
    nullable: true,
  })
  nomOrganisme: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  sigle: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  complementNom: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  adressePostale: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  complementAdresse: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  boitePostale: string;

  @Column({
    type: 'float',
    nullable: true,
  })
  codePostal: number;

  @Column({
    type: 'text',
    nullable: true,
  })
  commune: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  tel: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  tel2: string;

  @IsEmail()
  @Column({
    type: 'text',
    nullable: true,
  })
  email: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  url: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  descriptionActivite: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  ageEnClair: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  niveauActivite: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  momentActivite: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  tarif: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  contactActivite: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  lieuActivite: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  activite: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  paiementsAcceptes: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  age: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  niveau: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  communeOuQuartierDeLactivite: string;

  @ManyToOne(() => Coordonnee)
  @JoinColumn({ name: 'coordonneeId' })
  coordonnee: Coordonnee;

  @Column({ nullable: true })
  coordonneeId: number;
}
