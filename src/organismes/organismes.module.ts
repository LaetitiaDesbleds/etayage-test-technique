import { Module } from '@nestjs/common';
import { OrganismesService } from './organismes.service';
import { OrganismesController } from './organismes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Organisme } from './entities/organisme.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Organisme])], // Importe le module TypeOrm pour l'entité Organisme, ce qui permet de l'injecter dans les services ou les contrôleurs
  controllers: [OrganismesController],
  providers: [OrganismesService],
})
export class OrganismesModule {}
