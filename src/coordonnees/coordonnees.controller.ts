import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
} from '@nestjs/common';
import { CoordonneesService } from './coordonnees.service';
import { CreateCoordonneeDto } from './dto/create-coordonnee.dto';
import { UpdateCoordonneeDto } from './dto/update-coordonnee.dto';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('coordonnees')
@ApiTags('coordonnees')
export class CoordonneesController {
  constructor(private readonly coordonneesService: CoordonneesService) {}

  @Get()
  @ApiResponse({
    status: 201,
    description: 'La liste des coordonnées des organismes',
  })
  @ApiResponse({
    status: 404,
    description: 'Pas de liste trouvée',
  })
  findAll() {
    return this.coordonneesService.findAll();
  }

  @Get('/:id')
  @ApiResponse({
    status: 201,
    description: "Les coordonnées de l'organisme de loisirs",
  })
  @ApiResponse({
    status: 404,
    description: 'Pas de coordonnée',
  })
  findOne(@Param('id') id: number) {
    return this.coordonneesService.findOne(id);
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Les coordonnées ont bien été créées',
  })
  @ApiResponse({
    status: 404,
    description: 'Erreur lors de la création des coordonnées',
  })
  create(@Body() createCoordonneeDto: CreateCoordonneeDto) {
    return this.coordonneesService.create(createCoordonneeDto);
  }

  @Put('/:id')
  @ApiResponse({
    status: 201,
    description: 'Les coordonnées ont bien été modifiées',
  })
  @ApiResponse({
    status: 404,
    description: 'Erreur lors de la modification des coordonnées',
  })
  update(
    @Param('id') id: number,
    @Body() updateCoordonneeDto: UpdateCoordonneeDto,
  ) {
    return this.coordonneesService.update(id, updateCoordonneeDto);
  }

  @Delete('/:id')
  @ApiResponse({
    status: 201,
    description: 'Les coordonnées ont bien été supprimées',
  })
  @ApiResponse({
    status: 404,
    description: 'Erreur lors de la suppression de des coordonnées',
  })
  remove(@Param('id') id: number) {
    return this.coordonneesService.remove(id);
  }
}
