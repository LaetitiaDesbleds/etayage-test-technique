import { Module } from '@nestjs/common';
import { CoordonneesService } from './coordonnees.service';
import { CoordonneesController } from './coordonnees.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Coordonnee } from './entities/coordonnee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Coordonnee])],
  controllers: [CoordonneesController],
  providers: [CoordonneesService],
})
export class CoordonneesModule {}
