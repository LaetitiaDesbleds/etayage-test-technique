import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Organisme } from '../../organismes/entities/organisme.entity';

@Entity('coordonnees')
export class Coordonnee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'text',
    nullable: true,
  })
  communeOuQuartierDeLactivite: string;

  @Column({
    type: 'float',
    nullable: true,
  })
  latitude: number;

  @Column({
    type: 'float',
    nullable: true,
  })
  longitude: number;

  @OneToMany(() => Organisme, (organisme) => organisme.coordonnee)
  organismes: Organisme[];
}
