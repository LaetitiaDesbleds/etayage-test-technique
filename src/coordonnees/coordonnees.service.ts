import { Injectable } from '@nestjs/common';
import { CreateCoordonneeDto } from './dto/create-coordonnee.dto';
import { UpdateCoordonneeDto } from './dto/update-coordonnee.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Coordonnee } from './entities/coordonnee.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CoordonneesService {
  constructor(
    @InjectRepository(Coordonnee)
    private readonly coordonneRepository: Repository<Coordonnee>,
  ) {}
  async findAll(): Promise<Coordonnee[]> {
    return await this.coordonneRepository.find();
  }

  async findOne(id: number): Promise<Coordonnee> {
    return await this.coordonneRepository.findOne({
      where: {
        id,
      },
    });
  }

  async create(createCoordonneeDto: CreateCoordonneeDto) {
    const coordonneEntity = new Coordonnee();
    coordonneEntity.communeOuQuartierDeLactivite =
      createCoordonneeDto.communeOuQuartierDeLactivite;
    coordonneEntity.latitude = createCoordonneeDto.latitude;
    coordonneEntity.longitude = createCoordonneeDto.longitude;
    const coordonnee = this.coordonneRepository.create(coordonneEntity);
    await this.coordonneRepository.save(coordonnee);
    return coordonnee;
  }

  async update(id: number, data: Partial<UpdateCoordonneeDto>) {
    await this.coordonneRepository.update({ id }, data);
    const coordonnee = this.coordonneRepository.findOne({ where: { id } });
    return coordonnee;
  }

  async remove(id: number) {
    const coordonnee = this.coordonneRepository.findOne({ where: { id } });
    await this.coordonneRepository.delete({ id });
    return coordonnee;
  }
}
