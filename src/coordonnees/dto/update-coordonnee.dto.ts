import { PartialType } from '@nestjs/swagger';
import { CreateCoordonneeDto } from './create-coordonnee.dto';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class UpdateCoordonneeDto extends PartialType(CreateCoordonneeDto) {
  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly communeOuQuartierDeLactivite: string;

  @ApiModelProperty({
    required: false,
    type: 'float',
  })
  readonly latitude: number;

  @ApiModelProperty({
    required: false,
    type: 'float',
  })
  readonly longitude: number;
}
