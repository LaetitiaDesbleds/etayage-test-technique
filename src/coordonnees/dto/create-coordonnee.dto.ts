import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class CreateCoordonneeDto {
  @ApiModelProperty({
    required: false,
    type: 'string',
  })
  readonly communeOuQuartierDeLactivite: string;

  @ApiModelProperty({
    required: false,
    type: 'float',
  })
  readonly latitude: number;

  @ApiModelProperty({
    required: false,
    type: 'float',
  })
  readonly longitude: number;
}
