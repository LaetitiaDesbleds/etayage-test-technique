import { registerAs } from '@nestjs/config';
import { config as dotenvConfig } from 'dotenv';
import { DataSource, DataSourceOptions } from 'typeorm';

dotenvConfig({ path: '.env' });

// Définit la configuration de la base de données en utilisant les variables d'environnement
const config = {
  type: 'postgres',
  host: `${process.env.DATABASE_HOST}`,
  port: `${process.env.DATABASE_PORT}`,
  username: `${process.env.DATABASE_USERNAME}`,
  password: `${process.env.DATABASE_PASSWORD}`,
  database: `${process.env.DATABASE_NAME}`,
  entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  migrations: [__dirname + '/../migration/**/*{.ts,.js}'],
  subscribers: [__dirname + '/../subscriber/**/*.subscriber{.ts,.js}'],
  cli: {
    entitiesDir: 'src/entities',
    migrationsDir: 'src/migration',
    subscribersDir: 'src/subscriber',
  },
  autoLoadEntities: true,
  synchronize: true,
};

// Enregistre la configuration en tant que 'typeorm' pour pouvoir l'injecter plus tard
export default registerAs('typeorm', () => config);

// Crée une source de données à partir de la configuration
export const connectionSource = new DataSource(config as DataSourceOptions);
