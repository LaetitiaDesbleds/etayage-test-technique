import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  // Crée la configuration pour la documentation Swagger
  const config = new DocumentBuilder()
    .setTitle('Activités & Loisirs')
    .setDescription('Liste des activités et loisirs à Rennes')
    .setVersion('1.0')
    .addTag('organismes')
    .addTag('coordonnees')
    .addTag('Hello world default')
    .build();

  // Crée le document Swagger à partir de l'application et de la configuration
  // Met en place le module Swagger dans l'application pour servir la documentation à partir du chemin '/api'
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(3001);
}
bootstrap();
