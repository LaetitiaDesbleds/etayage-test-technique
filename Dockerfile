# Image de base
FROM node:18

# Créer le répertoire de l'application
WORKDIR /usr/src/app

# Copier les manifestes de dépendance des applications dans l'image du conteneur.
# Copier ceci en premier évite de relancer npm install à chaque changement de code.
# S'assurer de copier à la fois package.json ET package-lock.json (lorsqu'ils sont disponibles).
COPY --chown=node:node package*.json ./

# Installer les dépendances de l'application en utilisant la commande `npm ci` au lieu de `npm install`.
RUN npm ci

# Regrouper les sources de l'application
COPY --chown=node:node . .

# Port sur lequel l'application écoute
EXPOSE 3001

# Démarre le serveur
CMD [ "npm", "run", "start" ]