# ETAYAGE TEST TECHNIQUE BACK

Ce projet a été généré avec NestJs.

Il s'agit de la partie back-end du projet de test technique pour Etayage, créée avec **NestJs** et **Angular** pour le Front.

Vous trouverez le lien du référentiel de démarrage du framework TypeScript [Nest](https://github.com/nestjs/nest)

# Prérequis

Pour exécuter ce projet, vous devez avoir les logiciels suivants installés sur votre machine :

- Node.js et npm
- NestJS CLI
- Docker (optionnel, pour exécuter le projet dans un conteneur Docker)

## Installation

Suivez ces étapes pour installer le projet sur votre ordinateur :

1. Cloner le projet à partir de GitLab avec la commande suivante :

```bash
https://gitlab.com/LaetitiaDesbleds/etayage-test-technique.git
```

2. Installer les dépendances du projet avec npm :

```bash
$ npm install
```

## Build

Lancez la commande suivante pour construire le projet. Les artefacts de construction seront stockés dans le répertoire dist/.
```bash
npm run build 
```

## Exécution

Lancez la commande suivant pour démarrer le serveur. Il sera accessible à l'adresse http://localhost:3000/.
```bash
npm run start
```

Pour le démarrage en mode watch, utilisez :

```bash
$ npm run start:dev
```

## Docker

Lien Docker Hub : 
```bash
https://hub.docker.com/r/laetitiaepsi/etayage_test_technique-docker/tags
```

Si vous souhaitez exécuter ce projet dans un conteneur Docker, suivez ces étapes :

1.Construisez l'image Docker avec la commande suivante (la version 1.2 est la plus récente) :

    docker pull laetitiaepsi/etayage_test_technique-docker:1.2

2.Exécutez l'image Docker avec la commande suivante :

    docker run -p 3001:3001 laetitiaepsi/etayage_test_technique-docker:1.2

Ensuite, ouvrez votre navigateur web et visitez http://localhost:8080 pour voir l'application en action dans le conteneur Docker.

## Contact

Si vous avez des questions ou des commentaires sur ce projet,
veuillez me contacter à mon adresse email : **desbleds.laetitia@outlook.fr**
